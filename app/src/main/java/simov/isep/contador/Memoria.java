package simov.isep.contador;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Memoria {

    public static final String NOME_DEFAULT = "Algo";

    public static String nome = NOME_DEFAULT;

    public static List<Ponto> pontos = new ArrayList<Ponto>();

    public static Date dataInicio = Calendar.getInstance().getTime();
    public static Date dataLast = Calendar.getInstance().getTime();

    public static void iniciar(){
        nome = NOME_DEFAULT;
        pontos.clear();
        dataInicio = Calendar.getInstance().getTime();
        dataLast = dataInicio;
    }

    public static void marcar_normal(){
        marcar(true);
    }

    public static void marcar_prioritario(){
        marcar(false);
    }

    private static void marcar(boolean normal){
        Date now = Calendar.getInstance().getTime();

        long tempoDesdeInicio = 0;
        long tempoDesdeUltimo = 0;

        tempoDesdeInicio = now.getTime() - dataInicio.getTime();
        tempoDesdeUltimo = now.getTime() - dataLast.getTime();

        pontos.add(new Ponto(pontos.size() ,tempoDesdeUltimo, tempoDesdeInicio, now, normal));

        dataLast = now;
    }

    public static String filename(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HH-mm-ss_");
        String first = formatter.format(dataInicio);
        String last = nome.replaceAll("[^a-zA-Z0-9]", "");

        String fullname = first + last;
        return fullname;
    }

    public static String setName(String boxname){
        String last = boxname.replaceAll("[^a-zA-Z0-9]", "");
        nome = last;
        return new String(last);
    }





}
