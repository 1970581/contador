package simov.isep.contador;

import android.graphics.Color;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private static final String MY_SPACER = "****************************************************** ";

    TextView textView_count;
    TextView textView_hora;
    TextView textView_horalast;
    EditText editText_nome;

    Button bt_start;
    Button bt_record;
    Button bt_mark_normal;
    Button bt_mark_prioritario;

    boolean isLocked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView_count = (TextView) findViewById(R.id.activity_main_textview_count);
        textView_hora = (TextView) findViewById(R.id.activity_main_textview_hora);
        textView_horalast = (TextView) findViewById(R.id.activity_main_textview_horalast);


        editText_nome = (EditText) findViewById(R.id.activity_main_editText_nome);
        editText_nome.setText(Memoria.nome);
        updateCampos();


        bt_start = (Button) findViewById(R.id.main_button_start);
        bt_record = (Button) findViewById(R.id.main_button_record);
        bt_mark_normal = (Button) findViewById(R.id.main_button_mark_normal);
        bt_mark_prioritario = (Button) findViewById(R.id.main_button_mark_prioritario);

        bt_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Start
                unlock();
                Memoria.iniciar();
                String newName = Memoria.setName(editText_nome.getText().toString());
                editText_nome.setText(newName);
            }
        });

        bt_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //record
                lock();
                String filename = Memoria.filename();

                Memoria.nome = editText_nome.getText().toString();
                for(Ponto p : Memoria.pontos){
                    Log.i(TAG, MY_SPACER + " " + p.toString());
                }
                //Toast.makeText(MainActivity.this, "Saved. " + filename, Toast.LENGTH_LONG).show();

                saveFile();

            }
        });

        bt_mark_normal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //NORMAL
                Memoria.marcar_normal();
                updateCampos();
            }
        });

        bt_mark_prioritario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //NORMAL
                Memoria.marcar_prioritario();
                updateCampos();
            }
        });

        lock();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
/*
    public void lock(){ isLocked = true;}
    public void unlock(){ isLocked = false;}
  */

    public void lock(){
        //this.bt_mark_normal.setEnabled(false);
        //this.bt_mark_prioritario.setEnabled(false);
        //this.bt_record.setEnabled(false);
    }

    public void unlock(){
        //this.bt_mark_normal.setEnabled(true);
        //this.bt_mark_prioritario.setEnabled(true);
        //this.bt_record.setEnabled(true);
        bt_mark_normal.setBackgroundColor(Color.GREEN);
        bt_mark_prioritario.setBackgroundColor(Color.RED);
    }


    public void updateCampos(){
        this.textView_count.setText(""+Memoria.pontos.size());

        //SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        this.textView_hora.setText(formatter.format(Memoria.dataInicio));
        this.textView_horalast.setText(formatter.format(Memoria.dataLast));

        //bt_mark_normal.setBackgroundColor(Color.GREEN);
        //bt_mark_prioritario.setBackgroundColor(Color.RED);
    }



    public void saveFile(){

        String filenameString = Memoria.filename() + ".txt";
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(dir, filenameString);


        try {
            PrintWriter writer = new PrintWriter(file, "UTF-8");
            writer.print(Ponto.HEADER + "\r\n");
            for (Ponto p : Memoria.pontos){
                writer.print(p.toString());
                writer.print("\r\n");
            }
            writer.flush();
            writer.close();
            Toast.makeText(MainActivity.this, "Saved: " + filenameString, Toast.LENGTH_LONG).show();
        }
        catch(Exception ex0){
            Toast.makeText(MainActivity.this, "Error saving. " + filenameString, Toast.LENGTH_LONG).show();
            Log.e(TAG, MY_SPACER + " " + ex0);
        }
        /*

        FileOutputStream stream;
        try {
             stream = new FileOutputStream(file);
            stream.write("text-to-write".getBytes());
        }
        catch(Exception ex){

        }

        try{
            stream.close();
        }
        catch(Exception ex2){

        }
        */

    }


}
