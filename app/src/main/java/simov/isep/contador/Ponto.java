package simov.isep.contador;

//import java.time.Duration;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Ponto {

    public static final String HEADER = "id;data;duracao;intervalo;normal";
    public int id;
    public final boolean normal;
    public final long tempoDesdeUltimo;
    public final long tempoDesdeInicio;
    public final Date instante;


    public Ponto(int id,long tempoDesdeUltimo, long tempoDesdeInicio, Date instante, boolean normal){
        this.id = id;
        this.tempoDesdeInicio = tempoDesdeInicio;
        this.tempoDesdeUltimo = tempoDesdeUltimo;
        this.instante =  instante;
        this.normal = normal;
    }

    //@androidx.annotation.NonNull
    @Override
    public String toString() {
        int nor = 0;
        if (!normal) nor = 1;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        return "" + id + ";" + formatter.format(instante) + ";" + tempoDesdeInicio + ";" + tempoDesdeUltimo + ";" + nor;
        //return super.toString();
    }
}
